from urllib.request import urlretrieve
import os
import warnings
import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt

from pmdarima import auto_arima


class countries_energy_mixes:
    def __init__(self, energy_mix_df):
        self.energy_mix_df = energy_mix_df

    def download_file(self):
        """
        Downloads a file from an URL into the Downloads folder on the main directory.

        Parameters
        ------------
        file_link: str
            A string containing the link to the file you wish to download.
        output_file: str
            A string containing the name of the output file. The default value is 'file.csv'
            at the location you are running the function.

        Returns
        ---------
        Nothing


        """

        file_link = "https://nyc3.digitaloceanspaces.com/owid-public/data/energy/owid-energy-data.csv"

        if not os.path.exists("Downloads"):
            os.makedirs("Downloads")

        target_file = os.path.join("Downloads", "countriesEnergy.csv")

        try:

            if not os.path.exists(target_file):
                urlretrieve(file_link, filename=target_file)
            else:
                print("File already exists!")

        except BaseException:
            raise Exception("Sorry, there seem to be a problem while downloading...")

        finally:
            globalEnergyDf = pd.read_csv(target_file)
            globalEnergyDf = globalEnergyDf[
                (globalEnergyDf.year >= 1970) & (globalEnergyDf.year < 2018)
            ]

            # Part 2 operations to get year column as index and dateTime type
            globalEnergyDf["year_int"] = globalEnergyDf["year"]
            globalEnergyDf["year"] = pd.to_datetime(globalEnergyDf["year"], format="%Y")

            globalEnergyDf.set_index("year")
            globalEnergyDf.sort_index(inplace=True)

            self.energy_mix_df = globalEnergyDf

    def consumption(self, country: str, normalized: bool = True):
        """
        Plots an area chart of the consumed energy mix since 1970 of the country given in
        'country'.
        Energy may be normalized or not.

        Parameters
        ---------------
        country: str
            The country whose energy consumption should be shown

        normalized: bool
            False if data should be normalized. True by defaut

        Returns
        ---------------
        energy_plot: plot
            The plot showing consumed energy mix since 1970 of the country

        """
        self.available_countries()

        if country not in self.country_list:
            raise ValueError("Variable 'country' does not exist")

        energysourceDF = self.energy_mix_df
        energysourceDF = energysourceDF[energysourceDF["country"] == country]

        energysourceDF = energysourceDF[
            [
                "year",
                "biofuel_consumption",
                "coal_consumption",
                "gas_consumption",
                "hydro_consumption",
                "nuclear_consumption",
                "oil_consumption",
                "other_renewable_consumption",
                "solar_consumption",
                "wind_consumption",
            ]
        ]

        energysourceDF.set_index("year", inplace=True)

        if normalized:
            energysourceDF = (
                energysourceDF.div(energysourceDF.sum(axis=1), axis=0) * 100
            )

        energysourceDF = energysourceDF.dropna()
        energysourceDF.plot.area(
            legend=True, figsize=[10, 5], title=country, colormap="tab20b"
        ).legend(loc="center left", bbox_to_anchor=(1.0, 0.5))

    def compare_gdp(self, list_of_countries: list):
        """
        Plots a grpah containing the evolution of the GDPs of the given countries throughout the years.

        Parameters
        ---------------
        list_of_countries: list
            The country whose energy consumption should be shown

        Returns
        ---------------
        Nothing, just plots the information.

        """

        self.available_countries()

        for country in list_of_countries:
            if country not in self.country_list:
                raise ValueError("Variable 'country' does not exist")

        df_new = (
            self.energy_mix_df[self.energy_mix_df["country"].isin(list_of_countries)]
            .groupby(["country", "gdp", "year"])
            .sum()
            .reset_index(level=["country", "gdp", "year"])
            .sort_values(by=["year"], ascending=True)
        )

        df_new.set_index("year", inplace=True)
        df_new.groupby("country")["gdp"].plot(legend=True)

    def available_countries(self):
        """
        Outputs a list of the available countries in the data set.

        Parameters
        ------------
        Nothing.

        Returns
        ---------
        Returns the list of countries available on the data set energy_mix_df.

        """

        country_list = list(self.energy_mix_df["country"].unique())
        self.country_list = [
            e
            for e in country_list
            if e
            not in (
                "Africa",
                "Asia Pacific",
                "CIS",
                "Central America",
                "Eastern Africa",
                "Europe",
                "Europe (other)",
                "Middle Africa",
                "Middle East",
                "North America",
                "OPEC",
                "Other Asia & Pacific",
                "Other CIS",
                "Other Caribbean",
                "Other Middle East",
                "Other Northern Africa",
                "Other South America",
                "Other Southern Africa",
                "South & Central America",
                "USSR",
                "Western Africa",
                "Western Sahara",
                "World",
            )
        ]

        return self.country_list

    def compare_consumption(self, list_of_countries: list, plot: bool):
        """
        Compares the total of the comsumption columns for each chosen country on the list_of_countries and plot, so a comparison can be made.

        Parameters
        ------------
        list_of_countries: string with a country or a list of country strings.

        Returns
        ---------
        A line graph with the comparison of the total consumption for the countries chosen.
        """

        warnings.filterwarnings("ignore")

        self.available_countries()

        for country in list_of_countries:
            if country not in self.country_list:
                raise ValueError("Variable 'country' does not exist")

        df_c = self.energy_mix_df.loc[
            :, ["consumption" in i for i in self.energy_mix_df.columns]
        ]
        df_c.drop(
            [
                "renewables_consumption",
                "fossil_fuel_consumption",
                "other_renewable_consumption",
                "primary_energy_consumption",
            ],
            axis=1,
        )
        df_c["Total"] = df_c.sum(axis=1)

        self.energy_mix_df["Total_consumption"] = df_c["Total"]

        df_c1 = (
            self.energy_mix_df[self.energy_mix_df["country"].isin(list_of_countries)]
            .groupby(["country", "Total_consumption", "year"])
            .sum()
            .reset_index(level=["country", "Total_consumption", "year"])
            .sort_values(by=["year"], ascending=True)
        )
        df_c1.set_index("year", inplace=True)

        if plot:
            df_c1.groupby("country")["Total_consumption"].plot(legend=True)
        return df_c1

    def compare_consumption_and_emissions(self, list_of_countries: list, plot: bool):
        """
        Compares the total of the comsumption and emissions columns for each chosen country on the list_of_countries and plot, so a comparison can be made.

        Parameters
        ------------
        list_of_countries: string with a country or a list of country strings.

        Returns
        ---------
        A line graph with the comparison of the total consumption and emissions for the countries chosen.
        """

        warnings.filterwarnings("ignore")

        self.available_countries()

        for country in list_of_countries:
            if country not in self.country_list:
                raise ValueError("Variable 'country' does not exist")

        df_c = self.energy_mix_df.loc[
            :, ["consumption" in i for i in self.energy_mix_df.columns]
        ]
        df_c.drop(
            [
                "renewables_consumption",
                "fossil_fuel_consumption",
                "other_renewable_consumption",
                "primary_energy_consumption",
            ],
            axis=1,
        )
        df_c["Total"] = df_c.sum(axis=1)

        self.energy_mix_df["Total_consumption"] = df_c["Total"]

        df_c1 = (
            self.energy_mix_df[self.energy_mix_df["country"].isin(list_of_countries)]
            .groupby(["country", "Total_consumption", "year"])
            .sum()
            .reset_index(level=["country", "Total_consumption", "year"])
            .sort_values(by=["year"], ascending=True)
        )
        df_c1.set_index("year", inplace=True)

        dataf1 = self.energy_mix_df.loc[
            :,
            [
                "biofuel_consumption",
                "coal_consumption",
                "gas_consumption",
                "hydro_consumption",
                "nuclear_consumption",
                "oil_consumption",
                "solar_consumption",
                "wind_consumption",
            ],
        ]

        dataf1["biofuel"] = dataf1["biofuel_consumption"] * 1450 / 1e6 * 1e9
        dataf1["coal"] = dataf1["coal_consumption"] * 1000 / 1e6 * 1e9
        dataf1["gas"] = dataf1["gas_consumption"] * 455 / 1e6 * 1e9
        dataf1["hydro"] = dataf1["hydro_consumption"] * 90 / 1e6 * 1e9
        dataf1["nuclear"] = dataf1["nuclear_consumption"] * 5.5 / 1e6 * 1e9
        dataf1["oil"] = dataf1["oil_consumption"] * 1200 / 1e6 * 1e9
        dataf1["solar"] = dataf1["solar_consumption"] * 53 / 1e6 * 1e9
        dataf1["wind"] = dataf1["wind_consumption"] * 14 / 1e6 * 1e9

        dataf1.drop(
            [
                "biofuel_consumption",
                "coal_consumption",
                "gas_consumption",
                "hydro_consumption",
                "nuclear_consumption",
                "oil_consumption",
                "solar_consumption",
                "wind_consumption",
            ],
            inplace=True,
            axis=1,
        )
        dataf1["emissions"] = dataf1.sum(axis=1)

        self.energy_mix_df["Total_emissions"] = dataf1["emissions"]

        df_e = (
            self.energy_mix_df[self.energy_mix_df["country"].isin(list_of_countries)]
            .groupby(["country", "year", "Total_emissions"])
            .sum()
            .reset_index(level=["country", "year", "Total_emissions"])
            .sort_values(by=["year"], ascending=True)
        )

        table = pd.pivot_table(
            df_e,
            values=["Total_consumption", "Total_emissions"],
            index=["year"],
            columns=["country"],
        )

        if plot:

            fig, ax1 = plt.subplots()
            sns.lineplot(data=table.Total_consumption)
            ax2 = plt.twinx()
            sns.lineplot(data=table.Total_emissions, ax=ax2, dashes=False)
            ax1.set_ylabel("Consumption")
            ax2.set_ylabel("Emissions")
            ax1.lines[0].set_linestyle("solid")
            ax2.lines[0].set_linestyle("dashed")

        return df_e
        return df_c1

    def gapminder_(self, chosen_year: int):
        """
        Plot a scatter plot for a specific year where:
            - x-axis is gdp
            - y-axis is total energy consumption
            - area of each dot is the population
       

        Parameters
        ------------
        chosen_year: must be an integer, otherwise a type error is raised.
            The year from which we want the information.

        Returns
        ---------
        A scatter plot for a specific year (chosen_year) where:
            - x-axis is gdp
            - y-axis is total energy consumption
            - area of each dot is the population
        """
        warnings.filterwarnings("ignore")

        if not isinstance(chosen_year, int):
            raise TypeError("Variable 'chosen_year' is not int.")

        else:

            df_c = self.energy_mix_df.loc[
                :, ["consumption" in i for i in self.energy_mix_df.columns]
            ]
            df_c["Total"] = df_c.sum(axis=1)

            self.energy_mix_df["Total_consumption"] = df_c["Total"]

            df_6 = (
                self.energy_mix_df[
                    [
                        "country",
                        "Total_consumption",
                        "year",
                        "year_int",
                        "gdp",
                        "population",
                    ]
                ]
                .groupby(["country", "year", "population"])
                .sum()
                .reset_index(level=["country", "year", "population"])
                .sort_values(by=["year"], ascending=True)
            )

            df_7 = df_6[df_6["year_int"] == chosen_year]

            plt.figure(dpi=100)

            np_pop = np.array(df_7.population)
            np_pop2 = np_pop * 2

            sns.scatterplot(
                df_7["gdp"],
                df_7["Total_consumption"],
                hue=df_7["country"],
                size=np_pop2,
                sizes=(20, 400),
                legend=False,
                cmap="Paired",
                alpha=0.8,
                edgecolors="white",
                linewidth=1,
            )

            plt.grid(True)
            plt.xscale("log")
            plt.yscale("log")
            plt.xlabel("GDP", fontsize=9)
            plt.ylabel("Total Consumption", fontsize=9)
            plt.title(
                "GDP and Total energy consumption per country in a year", fontsize=9
            )

            plt.show()

    def compare_emissions(self, list_of_countries: list, plot: bool):
        """
        Compares the total of the emissions for each chosen country on the list_of_countries and plot, so a comparison can be made.

        Parameters
        ------------
        list_of_countries: string with a country or a list of country strings.

        Returns
        ---------
        A line graph with the comparison of the total emissions for the countries chosen.
        """

        dataf1 = self.energy_mix_df.loc[
            :,
            [
                "biofuel_consumption",
                "coal_consumption",
                "gas_consumption",
                "hydro_consumption",
                "nuclear_consumption",
                "oil_consumption",
                "solar_consumption",
                "wind_consumption",
            ],
        ]

        dataf1["biofuel"] = dataf1["biofuel_consumption"] * 1450 / 1e6 * 1e9
        dataf1["coal"] = dataf1["coal_consumption"] * 1000 / 1e6 * 1e9
        dataf1["gas"] = dataf1["gas_consumption"] * 455 / 1e6 * 1e9
        dataf1["hydro"] = dataf1["hydro_consumption"] * 90 / 1e6 * 1e9
        dataf1["nuclear"] = dataf1["nuclear_consumption"] * 5.5 / 1e6 * 1e9
        dataf1["oil"] = dataf1["oil_consumption"] * 1200 / 1e6 * 1e9
        dataf1["solar"] = dataf1["solar_consumption"] * 53 / 1e6 * 1e9
        dataf1["wind"] = dataf1["wind_consumption"] * 14 / 1e6 * 1e9

        dataf1.drop(
            [
                "biofuel_consumption",
                "coal_consumption",
                "gas_consumption",
                "hydro_consumption",
                "nuclear_consumption",
                "oil_consumption",
                "solar_consumption",
                "wind_consumption",
            ],
            inplace=True,
            axis=1,
        )
        dataf1["emissions"] = dataf1.sum(axis=1)

        self.energy_mix_df["Total_emissions"] = dataf1["emissions"]

        df_e = (
            self.energy_mix_df[self.energy_mix_df["country"].isin(list_of_countries)]
            .groupby(["country", "year", "Total_emissions"])
            .sum()
            .reset_index(level=["country", "year", "Total_emissions"])
            .sort_values(by=["year"], ascending=True)
        )
        df_e.set_index("year", inplace=True)

        if plot:
            df_e.groupby("country")["Total_emissions"].plot(legend=True)

        return df_e

    def predict_consumption(self, list_of_countries: list, predicted_periods: int):
        """
        Given a specific country by its name, and a number of predicted periods (years), this method plots a graph with the consumption of
         that country throughout time, plus, on a different color, a prediction on how the consumption will
        evolve using auto-ARIMA algorithm.

        Parameters
        ------------
        list_of_countries: list
            A list of strings containing only one country. If this list is larger than 1, an exception will be raise.
        predicted_periods: int
            Number of predicted periods (years) to be ploted on the final graph

        Returns
        ---------
        Nothing


        """

        warnings.filterwarnings("ignore")

        # Countries names validation
        self.available_countries()
        for country in list_of_countries:
            if country not in self.country_list:
                raise ValueError("Variable 'country' does not exist.")

        if predicted_periods < 1:
            raise ValueError("Number of predicted periods must be greater than 1.")

        if len(list_of_countries) > 1:
            raise ValueError("Number of inserted countries is greater than 1.")

        # Building datasets for predictions
        df_consumption = pd.DataFrame()
        df_consumption["Total_Consumption"] = self.compare_consumption(
            list_of_countries, False
        ).Total_consumption

        # Building a orediction df baseed on auto ARIMA aplication
        prediction_consumptions = self.build_arima_prediction(
            df_consumption, predicted_periods, "consumptions"
        )

        # Ploting Consimption and predicted Consumption
        fig, ax = plt.subplots(figsize=(12, 5))
        df_consumption.plot(ax=ax, legend="Train")
        prediction_consumptions.plot(ax=ax, label="Consumption Prediction")
        plt.legend(["Train", "Consumption Prediction"])
        plt.title("Consumption prediction for " + list_of_countries[0])
        plt.tight_layout()
        plt.show()

    def predict_emissions(self, list_of_countries: list, predicted_periods: int):
        """
        Given a specific country by its name, and a number of predicted periods (years), this method plots a graph with the emissions of
         that country throughout time, plus, on a different color, a prediction on how the emissions will
        evolve using auto-ARIMA algorithm.

        Parameters
        ------------
        list_of_countries: list
            A list of strings containing only one country. If this list is larger than 1, an exception will be raise.
        predicted_periods: int
            Number of predicted periods (years) to be ploted on the final graph.

        Returns
        ---------
        Nothing


        """

        warnings.filterwarnings("ignore")

        # Countries names validation
        self.available_countries()
        for country in list_of_countries:
            if country not in self.country_list:
                raise ValueError("Variable 'country' does not exist.")

        if predicted_periods < 1:
            raise ValueError("Number of predicted periods must be greater than 1.")

        if len(list_of_countries) > 1:
            raise ValueError("Number of inserted countries is greater than 1.")

        # Building datasets for predictions
        df_emissions = pd.DataFrame()
        df_emissions["Total_emissions"] = self.compare_emissions(
            list_of_countries, False
        ).Total_emissions

        # Building a orediction df baseed on auto ARIMA aplication
        prediction_emissions = self.build_arima_prediction(
            df_emissions, predicted_periods, "emissions"
        )

        # Ploting Consimption and predicted Consumption
        fig, ax = plt.subplots(figsize=(12, 5))
        df_emissions.plot(ax=ax, legend="Train")
        prediction_emissions.plot(ax=ax, label="Emissions Prediction")
        plt.legend(["Train", "Emissions Prediction"])
        plt.title("Emissions prediction for " + list_of_countries[0])
        plt.tight_layout()
        plt.show()

    def build_arima_prediction(
        self, df: pd.DataFrame, predicted_periods: int, data_type: str
    ):
        """
        Applies arima algorithm.

        Parameters
        ------------
        df: pd.DataFrame
            A dataframe where thi information for the ARIMA application is.

        predicted_periods: int
            number of predicted periods

        data_type: str
            To ensure that we apply the algorithm properly for emissions and sonsumptions, this string will filter the application of the algo
            between emissions and consumption



        Returns
        ---------
        prediction: array


        """
        target_column = []

        if data_type == "emissions":
            target_column = df["Total_emissions"]
        else:
            target_column = df["Total_Consumption"]

        prediction_wise_fit = auto_arima(
            target_column,
            start_p=1,
            start_q=1,
            max_p=3,
            max_q=3,
            m=12,
            start_P=0,
            seasonal=True,
            d=None,
            D=1,
            trace=True,
            error_action="ignore",  # Ignore incompatible settings
            suppress_warnings=True,
            stepwise=True,
        )

        prediction = pd.DataFrame(
            prediction_wise_fit.predict(n_periods=predicted_periods)
        )

        prediction["Time"] = pd.date_range(
            start="2017-01-01", periods=predicted_periods, freq="Y"
        )

        prediction.set_index("Time", inplace=True)

        return prediction

    def plot_emissions_consumption(self):

        """
        Plots a scatterplot of average annual energy emissions and energy consumption between 2011 and 2016 of all countries
    
        Parameters
        ---------------
        none
        
        Returns
        ---------------
        A scatterplot
    
        """
        energy_mix_df_ec = self.energy_mix_df
        threshold_year = pd.to_datetime("2010", format="%Y")
        energy_mix_df_ec = energy_mix_df_ec[energy_mix_df_ec["year"] > threshold_year]

        energy_mix_df_ec = energy_mix_df_ec[energy_mix_df_ec['country'].isin(self.country_list)]
        energy_mix_df_ec = energy_mix_df_ec[
            ["country", "population", "Total_consumption", "Total_emissions"]
        ]
        energy_mix_df_ec = energy_mix_df_ec.dropna().groupby(["country"]).mean()

        sns.set(rc={"figure.figsize": (15, 8)})
        sns.scatterplot(
            data=energy_mix_df_ec,
            x="Total_consumption",
            y="Total_emissions",
            hue="country",
            size=("population"),
            sizes=(100, 20000),
            alpha=0.3,
            legend=False,
            
        )
        
        plt.xscale("log")
        plt.yscale("log")
        plt.xlabel("Consumption")
        plt.ylabel("Emissions")
        plt.title(
            "Average Annual Emissions and Consumptions from 2011 to 2016 per Country"
        )
