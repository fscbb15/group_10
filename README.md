# Group_10

Advanced Programing For Data Science: Group 10 Project

## Group Number
Group 10

## Class
Advanced Programming for Data Science

## Contributours
Felipe Botelho, 48740@novasbe.pt 
Sandra Keiser, 50337@novasbe.pt 
Margarida Pinto Basto, 45922novasbe.pt 
Maria Veiga, 39346@novasbe.pt 

## Getting started
In order to take the mos out of group_10 project, just run all cell on the showcase notebook.


## Name
Countries Energy Mixes for Norway, Portugal and Argentina

## Description
The Energy Mixes project, is a masters project of Nova sbe College on Advance Programming for Data Analysis course.
Its main goal is to show a deep presepective and all the possible insights any user can take out from energy consumption and its emissions
on severall countries accorss the globe.

## Authors and acknowledgment
Here is our appreciation to those who have contributed to the project:
Sandra Keiser
Felipe Botelho
Margarida Pinto Basto
Maria Veiga



## Project status
Completed.
